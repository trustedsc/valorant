(function($) {

    $(document).ready(function() {
    		
    		function saveInAnalytics(category, action, label) {
					if ("ga" in window) {
						if (typeof ga.getAll !== "undefined") { 
							tracker = ga.getAll()[0];
					    if (tracker) 
				        tracker.send('event', category, action, label);
						}
					}
    		}

    	  $(".download").click(function() {
					saveInAnalytics("clicks", "download", "csgo hack v1.9");
    	  });
    	  
    });

}(jQuery));